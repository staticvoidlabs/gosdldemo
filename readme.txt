


Links
----------------------------------------------------------------------------
https://github.com/velovix/gaming-in-go
https://gameswithgo.org/topics.html
https://madewithgolang.com/g3n-engine/



Setup SDL2
----------------------------------------------------------------------------

Follow these instructions: https://github.com/veandco/go-sdl2

Install mingw-w64 from Mingw-builds
 Version: latest (at time of writing 6.3.0)
 Architecture: x86_64
 Threads: win32
 Exception: seh
 Build revision: 1
 Destination Folder: Select a folder that your Windows user owns

http://libsdl.org/download-2.0.php > Download Development Libraries > SDL2-devel-2.0.12-mingw.tar.gz

Extract the SDL2 folder from the archive using a tool like 7zip
Inside the folder, copy the i686-w64-mingw32 and/or x86_64-w64-mingw32 depending on the architecture you chose into your mingw-w64 folder e.g. C:\Program Files\mingw-w64\x86_64-6.3.0-win32-seh-rt_v5-rev1\mingw64

Setup Path environment variable
Put your mingw-w64 binaries location into your system Path environment variable. e.g. C:\Program Files\mingw-w64\x86_64-6.3.0-win32-seh-rt_v5-rev1\mingw64\bin and C:\Program Files\mingw-w64\x86_64-6.3.0-win32-seh-rt_v5-rev1\mingw64\x86_64-w64-mingw32\bin

Open up a terminal such as Git Bash and run go get -v github.com/veandco/go-sdl2/sdl.

(Optional) You can repeat Step 2 for SDL_image, SDL_mixer, SDL_ttf
NOTE: pre-build the libraries for faster compilation by running go install github.com/veandco/go-sdl2/{sdl,img,mix,ttf}
Or you can install SDL2 via Msys2 like so: pacman -S mingw-w64-x86_64-gcc mingw-w64-x86_64-SDL2{,_image,_mixer,_ttf,_gfx}


