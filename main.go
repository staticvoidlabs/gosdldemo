package main

import (
	"fmt"

	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
)

const (
	screenWidth  = 600
	screenHeight = 800
)

var (
	solidTexture *sdl.Texture
	font         *ttf.Font
)

func main() {

	sdlflags := uint32(sdl.WINDOW_OPENGL)
	sdlflags |= sdl.WINDOW_BORDERLESS

	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		fmt.Println("initializing SDL:", err)
		return
	}

	window, err := sdl.CreateWindow(
		"Gaming in Go Episode 2",
		sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		screenWidth, screenHeight,
		sdlflags)
	if err != nil {
		fmt.Println("initializing window:", err)
		return
	}
	defer window.Destroy()

	err = window.SetWindowOpacity(0.8)

	if err != nil {
		fmt.Println("setting opacity:", err)
		return
	}

	renderer, err := sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		fmt.Println("initializing renderer:", err)
		return
	}
	defer renderer.Destroy()

	// Font Start
	if err := ttf.Init(); err != nil {
		fmt.Printf("Failed to initialize TTF: %s\n", err)
	}

	if font, err = ttf.OpenFont("fonts/calibril.ttf", 20); err != nil {
		fmt.Printf("Failed to open font: %s\n", err)
	}

	var solidSurface *sdl.Surface
	if solidSurface, err = font.RenderUTF8Blended("Kitchen Impossible.mp4", sdl.Color{255, 255, 255, 255}); err != nil {
		fmt.Printf("Failed to render text: %s\n", err)
	}

	//solidSurface.Free()

	if solidTexture, err = renderer.CreateTextureFromSurface(solidSurface); err != nil {
		fmt.Printf("Failed to create texture: %s\n", err)
	}
	// Font End

	plr, err := newPlayer(renderer)
	if err != nil {
		fmt.Println("creating player:", err)
		return
	}

	for {
		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {

			//switch event.(type) {
			switch t := event.(type) {

			case *sdl.QuitEvent:
				return

			case *sdl.KeyboardEvent:
				if t.State == sdl.RELEASED && t.Keysym.Sym == sdl.K_ESCAPE {
					fmt.Println("Key: Esc")
					return
				}
			}

		}

		//renderer.SetDrawColor(255, 255, 255, 255)
		renderer.SetDrawColor(10, 10, 10, 10)
		renderer.Clear()

		plr.draw(renderer)

		_, _, tmpW, tmpH, _ := solidTexture.Query()
		//renderer.Copy(solidTexture, nil, &sdl.Rect{X: 300, Y: 10, W: tmpW, H: tmpH})
		renderer.CopyEx(solidTexture, nil, &sdl.Rect{X: 300, Y: 10, W: tmpW, H: tmpH}, 45.0, nil, sdl.FLIP_NONE)

		renderer.Present()
	}
}
